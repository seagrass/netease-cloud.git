# NetEase-Cloud (网易云)

## 介绍
基于vue2仿的网易云音乐App 

## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## 说明
1、利用vue/cli搭建项目 <br>
2、得先下载依赖相关依赖 npm install <br>
3、启动项目 npm run serve <br>

## 效果图（部分）
<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%8E%A8%E8%8D%90%E9%9F%B3%E4%B9%90.png" alt="推荐音乐" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E7%83%AD%E6%AD%8C%E6%A6%9C.png" alt="热歌榜" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%90%9C%E7%B4%A2.png" alt="搜索" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%AD%8C%E5%8D%95%E8%AF%A6%E6%83%85%E9%A1%B5.png" alt="歌单详情页" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E4%B8%93%E8%BE%912.png" alt="专辑封面" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%AD%8C%E8%AF%8D%E9%A1%B5.png" alt="歌词页" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E8%AF%84%E8%AE%BA%E8%AF%A6%E6%83%85%E9%A1%B5.png" alt="评论详情页" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%90%9C%E7%B4%A2%E7%BB%93%E6%9E%9C%E9%A1%B5.png" alt="搜索结果页" height="300">

<img src="https://gitee.com/seagrass/netease-cloud/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/%E6%92%AD%E6%94%BE%E5%88%97%E8%A1%A8.png" alt="播放列表" height="300">

# 其它项目
- [蛋糕商城 + 论文管理系统 + 图书管理系统](https://gitee.com/seagrass/web.git)

- [简单人事管理系统](https://gitee.com/seagrass/PMsystem.git)